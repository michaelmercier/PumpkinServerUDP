﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace CMD_Controller
{
    public partial class MainWindow : Window
    {
        private int NbPlayer = 0;
        private int OldButtonSelected = 0;
        private int OpenServer_Time = 35000; // 20 secondes
        private int OpenClients_Time = 6000; // 6 secondes
        private int nbIP;

        private string LanguagePath;
        private string[] GamePath;
        private string[] IpList;
        private const string SHUTDOWN = "Shutdown";

        private bool GameStarted = false;
        private bool AppFinishedLoading = false;

        //Initalize classes.
        ProcessCDM cmd = new ProcessCDM();
        UDP_Controller udp = new UDP_Controller();

        public MainWindow()
        {
            InitializeComponent();
        }

        //When the window is loaded. 
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cmd.StartCMD();
            ReadFileIP();
            ReadFileGamePath();
            SetComboBox();

            AppFinishedLoading = true;

           // System.Windows.Forms.SendKeys.SendWait("{1}");
        }

        /// <summary>
        /// Open game. 
        /// </summary>
        private void bStart_Click(object sender, RoutedEventArgs e)
        {                  
            if (GameStarted != true)
            {
                if (NbPlayer != 0)
                {
                    imgOpen.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_02.png", UriKind.Relative));

                    DisableButtons();

                    GameStarted = true;

                    for (int i = 0; i < NbPlayer; i++)
                    {
                        udp.SendData(cmd.EXECUTE_GAME, IpList[i]);
                        if (i==0)
                            Thread.Sleep(OpenServer_Time);
                        else
                            Thread.Sleep(OpenClients_Time);
                    }

                    for (int i = 0; i < NbPlayer; i++)
                    {
                        udp.SendData(GamePath[0] + " +", IpList[i]);
                        Thread.Sleep(OpenClients_Time);
                    }
                }
                else
                {
                    MessageBox.Show("No player selected !", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                MessageBox.Show("The game is already started !", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// CLose all the games that are opened.
        /// </summary>
        private void bStop_Click(object sender, RoutedEventArgs e)
        {
            if (NbPlayer != 0 && GameStarted == true)
            {
                imgClose.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_02.png", UriKind.Relative));
                MessageBoxResult result = MessageBox.Show("Do you want to close the game ?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    EnableButtons();
                    imgOpen.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_01.png", UriKind.Relative));
                    imgStart.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_01.png", UriKind.Relative));

                    for (int i = 0; i < NbPlayer; i++)
                    {
                        udp.SendData(cmd.KILL_GAME, IpList[i]);
                    }

                    GameStarted = false;

                    imgClose.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_01.png", UriKind.Relative));
                }
                imgClose.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_01.png", UriKind.Relative));
            }
            else
            {
                MessageBox.Show("There's no game to close !", "Warning", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        /// <summary>
        /// Number of Player that will play the game
        /// </summary>
        private void bp1_Click(object sender, RoutedEventArgs e)
        {
            NbPlayer = 1;
            SetButtonColors(1);
        }

        private void bp2_Click(object sender, RoutedEventArgs e)
        {
            if (nbIP >= 1)
            {
                NbPlayer = 2;
                SetButtonColors(2);
            }
            else
            {
                NbPlayer = 0;
                MessageBox.Show("There's not enough IP !", "Warning", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void bp3_Click(object sender, RoutedEventArgs e)
        {
            if (nbIP >= 3)
            {
                NbPlayer = 3;
                SetButtonColors(3);
            }
            else
            {
                NbPlayer = 0;
                MessageBox.Show("There's not enough IP !", "Warning", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void bp4_Click(object sender, RoutedEventArgs e)
        {
            if (nbIP >= 4)
            {
                NbPlayer = 4;
                SetButtonColors(4);
            }
            else
            {
                NbPlayer = 0;
                MessageBox.Show("There's not enough IP !", "Warning", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void bp5_Click(object sender, RoutedEventArgs e)
        {
            if (nbIP >= 5)
            {
                NbPlayer = 5;
                SetButtonColors(5);
            }
            else
            {
                NbPlayer = 0;
                MessageBox.Show("There's not enough IP !", "Warning", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        /// <summary>
        /// Shutdown all computer that are registered into the list. 
        /// </summary>
        private void bShutDown_Click(object sender, RoutedEventArgs e)
        {
            imgShutdown.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_02.png", UriKind.Relative));
            if (NbPlayer != 1)
            {
                MessageBoxResult result = MessageBox.Show("Close all client's computers ?", "Shutdown", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    imgShutdown.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_01.png", UriKind.Relative));
                    for (int i = 0; i < nbIP; i++)
                    {
                        udp.SendData(SHUTDOWN, IpList[i]);
                        Thread.Sleep(OpenClients_Time);
                    }
                }
            }
            imgShutdown.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_01.png", UriKind.Relative));
        }

        /// <summary>
        /// Read .txt File to get the Ip adresses.
        /// </summary>
        private void ReadFileIP()
        {
            var list = new List<string>();          
            var fileStream = new FileStream(System.IO.Path.GetFullPath("ConnectedComputer.txt"), FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    list.Add(line);
                }
            }
            IpList = list.ToArray();
            nbIP = IpList.Count();

            for (int i = 0; i < nbIP; i++)
            {
                listBox.Items.Add(IpList[i]);
            }
        }

        /// <summary>
        /// Get the path of the game and insert it into a label.
        /// </summary>
        private void ReadFileGamePath()
        {
            GamePath = cmd.ReadFileGamePath();
            lbGamePath.Content = GamePath[0] + '\n' + GamePath[1];
            LanguagePath = GamePath[1];
            //Initialize values in cmd with the game path.
            cmd.InitializeValues();
        }

        /// <summary>
        /// Disable buttons when the game is started. 
        /// </summary>
        private void DisableButtons()
        {
            bp1.IsEnabled = false;
            bp2.IsEnabled = false;
            bp3.IsEnabled = false;
            bp4.IsEnabled = false;
            bp5.IsEnabled = false;

            cbPlayer1.IsEnabled = false;
            cbPlayer2.IsEnabled = false;
            cbPlayer3.IsEnabled = false;
            cbPlayer4.IsEnabled = false;
            cbPlayer5.IsEnabled = false;
        }

        /// <summary>
        /// Enable buttons when the game is over. 
        /// </summary>
        private void EnableButtons()
        {
            bp1.IsEnabled = true;
            bp2.IsEnabled = true;
            bp3.IsEnabled = true;
            bp4.IsEnabled = true;
            bp5.IsEnabled = true;

            cbPlayer1.IsEnabled = true;
            cbPlayer2.IsEnabled = true;
            cbPlayer3.IsEnabled = true;
            cbPlayer4.IsEnabled = true;
            cbPlayer5.IsEnabled = true;
        }

        /// <summary>
        /// Set the comboBox with the available language & set it to the first one by default.(chinese)
        /// </summary>
        private void SetComboBox()
        {
            List<string> myItemsCollection = new List<string>();
            myItemsCollection.Add("Chinese");
            myItemsCollection.Add("Korean");
            myItemsCollection.Add("Japanese");
            myItemsCollection.Add("English");

            cbPlayer1.ItemsSource = myItemsCollection;
            cbPlayer2.ItemsSource = myItemsCollection;
            cbPlayer3.ItemsSource = myItemsCollection;
            cbPlayer4.ItemsSource = myItemsCollection;
            cbPlayer5.ItemsSource = myItemsCollection;

            cbPlayer1.SelectedIndex = 0;
            cbPlayer2.SelectedIndex = 0;
            cbPlayer3.SelectedIndex = 0;
            cbPlayer4.SelectedIndex = 0;
            cbPlayer5.SelectedIndex = 0;
        }

        /// <summary>
        /// Change button effect when clicking.
        /// </summary>
        private void SetButtonColors(int iButton)
        { 
            if (iButton == 1)
            {
                imgBtn1.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_02.png", UriKind.Relative));
            }
            else if (iButton == 2)
            {
                imgBtn2.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_02.png", UriKind.Relative));
            }
            else if (iButton == 3)
            {
                imgBtn3.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_02.png", UriKind.Relative));
            }
            else if (iButton == 4)
            {
                imgBtn4.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_02.png", UriKind.Relative));
            }
            else if (iButton == 5)
            {
                imgBtn5.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_02.png", UriKind.Relative));
            }

            if (iButton != OldButtonSelected)
            {
                switch (OldButtonSelected)
                {
                    case 0:
                        OldButtonSelected = iButton;
                        break;

                    case 1:
                        imgBtn1.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_01.png", UriKind.Relative));
                        OldButtonSelected = iButton;
                        break;

                    case 2:
                        imgBtn2.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_01.png", UriKind.Relative));
                        OldButtonSelected = iButton;
                        break;

                    case 3:
                        imgBtn3.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_01.png", UriKind.Relative));
                        OldButtonSelected = iButton;
                        break;

                    case 4:
                        imgBtn4.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_01.png", UriKind.Relative));
                        OldButtonSelected = iButton;
                        break;

                    case 5:
                        imgBtn5.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_01.png", UriKind.Relative));
                        OldButtonSelected = iButton;
                        break;
                }
            }
        }

        /// <summary>
        /// Start the game with a .wfs file that will simulate keyboard. (Number of player + enter)
        /// </summary>
        private void bStartGame_Click(object sender, RoutedEventArgs e)
        {
            if (GameStarted == true)
            {
                imgStart.Source = new BitmapImage(new Uri(@"/Images/AI_Button_Rectangle_02.png", UriKind.Relative));
                string sFile = "";
                sFile = "call " + "C:/ClientController/WSF/Player" + NbPlayer + ".wsf";

                udp.SendData(sFile, IpList[0]);
            }
            else
                MessageBox.Show("The game isn't open!", "Warning", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        /// <summary>
        /// Set languages for all players.
        /// </summary>
        private void cbPlayer1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string SelectedLanguage = cbPlayer1.SelectedItem.ToString();  
            DefaultLanguage(SelectedLanguage, 0);       
        }

        private void cbPlayer2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string SelectedLanguage = cbPlayer2.SelectedItem.ToString();
            DefaultLanguage(SelectedLanguage, 1);
        }

        private void cbPlayer3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string SelectedLanguage = cbPlayer3.SelectedItem.ToString();
            DefaultLanguage(SelectedLanguage, 2);       
        }

        private void cbPlayer4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string SelectedLanguage = cbPlayer4.SelectedItem.ToString();
            DefaultLanguage(SelectedLanguage, 3);
        }

        private void cbPlayer5_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string SelectedLanguage = cbPlayer5.SelectedItem.ToString();
            DefaultLanguage(SelectedLanguage, 4);
        }

        /// <summary>
        /// Default language before starting the game. (Chinese)
        /// </summary>
        public void DefaultLanguage(string language, int i)
        {
            if (AppFinishedLoading == true)
            {
                string LanguageInfo = language + "|" + LanguagePath;
                udp.SendData(LanguageInfo, IpList[i]);
            }
            else
                try
                {
                    udp.SendData("Chinese" + "|" + LanguagePath, IpList[i]);
                }
                catch { }
        }
    }
}
