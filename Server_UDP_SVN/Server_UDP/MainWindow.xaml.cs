﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace Server_UDP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ProcessCDM cmd = new ProcessCDM();

        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Looping server waiting for informations to come in. 
        /// </summary>
        public void serverThread()
        {
            UdpClient udpClient = new UdpClient(80);
            string sData = "";

            while (true)
            {
                IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
                Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                string returnData = Encoding.ASCII.GetString(receiveBytes);

                sData = (returnData).ToString();

                if (sData.Contains('|'))
                    cmd.ChangeGameLanguage(sData);

                else if (sData == "Shutdown")
                    cmd.ShutdownComputer();
             
                else if (sData.Contains('+'))
                    cmd.SelectGameWindow(sData);

                else
                    cmd.ExecuteCommands(sData);   
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cmd.StartCMD();

            Thread thdUDPServer = new Thread(new ThreadStart(serverThread));
            thdUDPServer.Start();

            WindowState = System.Windows.WindowState.Minimized;
            this.Hide();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            cmd.KillApp();
            System.Windows.Application.Current.Shutdown();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            cmd.KillApp();
            System.Windows.Application.Current.Shutdown();
        }
    }
}
